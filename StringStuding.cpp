
#include <iostream>
#include <string>

int main()
{   
    std::cout << "Enter any text: " << "\t";
    
    // ������������� ���������� � ������ ������ � ��
    std::string Text;
    std::getline(std::cin, Text);

    // ����� ��������� �������� � �������� �������:
    // ����� ����� ������
    std::cout << "Length of your text is: " << Text.length() << "\n";

    // ����� ������� ������� ������
    std::cout << "The first symbol is: " << "\t" << Text[0] << "\n";

    // ����� ���������� ������� ������ 
    // (�������� �� ������� ������� 1, �.�. ������� ���������� � 0)
    std::cout << "The last symbol is:  " << "\t" << Text[Text.length() - 1] << "\n";


    return 0;
}